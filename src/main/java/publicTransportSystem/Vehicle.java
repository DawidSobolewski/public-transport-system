package publicTransportSystem;

import java.util.ArrayList;
import java.util.List;

public class Vehicle {
    protected static String venture = "MPK Cracow";
    protected Integer number;
    protected List<Stop> route;

    public Vehicle(Integer number) {
        this.number = number;
    }

    public Vehicle(Integer number, List<Stop> route) {
        this.number = number;
        this.route = new ArrayList<Stop>();
    }

    public void addBusStop(String stopName, String nextStop, int distanceToNextStop, int durationToNextStop) {
        Stop stop = new Stop(stopName, nextStop, distanceToNextStop, durationToNextStop);
        route.add(stop);

    }

    public void addBusStop(String stopName, String nextStop, int distanceToNextStop, int durationToNextStop, String... fdas) {
        Stop stop;
        stop = new TransferStop(stopName, nextStop, distanceToNextStop, durationToNextStop, fdas);
        route.add(stop);

    }


    public void addBusStop(String busName) {
        Stop stop = new Stop(busName);
        route.add(stop);
    }

    public void addBusStop(String busName, String... p) {
        Stop stop;
        stop = new TransferStop(busName, p);
        route.add(stop);
    }

    @Override
    public String toString() {
        return "Owner " + venture +
                ", number " + number +
                ", route " + route;
    }
}
