package publicTransportSystem;


public class Stop {

    protected String stopName = null;
    protected String nextStop = null;

    protected int distanceToNextStop = 0;
    protected int durationToNextStop = 0;

    public Stop(String stopName, String nextStop, int distanceToNextStop, int durationToNextStop) {
        this.stopName = stopName;
        this.nextStop = nextStop;
        this.distanceToNextStop = distanceToNextStop;
        this.durationToNextStop = durationToNextStop;
    }

    public Stop(String stopName) {
        this.stopName = stopName;
    }

    @Override
    public String toString() {
        return "\nBus stop: " + stopName + "\n" + "Next stop: " + ((nextStop == null) ? "there is no next bus stop" : nextStop) + "\n" +
                "Distance to next bus stop: " + distanceToNextStop + " metres \n" + "Duration to next bus stop: " + durationToNextStop + " minutes \n";
    }

    public String getStopName() {
        return stopName;
    }

    public String getNextStop() {
        return nextStop;
    }

    public int getDistanceToNextStop() {
        return distanceToNextStop;
    }

    public int getDurationToNextStop() {
        return durationToNextStop;
    }
}
