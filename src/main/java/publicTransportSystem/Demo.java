package publicTransportSystem;

import java.util.ArrayList;
import java.util.List;

public class Demo {
    public static void main(String[] args) {

        List list = new ArrayList<Stop>();
        Vehicle vehicle1 = new Vehicle(4, list);
        Vehicle vehicle2 = new Vehicle(129, list);

        vehicle1.addBusStop("Rondo Grzegórzeckie", "Rondo Mogilskie", 300, 2, "Bieżanów", "Prokocim, Kurdwanów", "Salwator", "Bronowice");
        vehicle1.addBusStop("Starowiślna");
        vehicle1.addBusStop("Grota - Roweckiego", "Norymberska", 500, 3);

        vehicle2.addBusStop("Poczta Główna", "Galeria Krakowska", "Stradom", "Plac Wszystkich Świętych");
        vehicle2.addBusStop("Czerwone maki");

        System.out.println(vehicle1);
        System.out.println(vehicle2);

    }
}
