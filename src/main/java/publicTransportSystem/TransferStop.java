package publicTransportSystem;

public class TransferStop extends Stop {

    protected String[] changes;

    public TransferStop() {
        super("-", null, 0, 0);
        this.changes = null;
    }

    public TransferStop(String stopName, String nextStop, int distanceToNextStop, int durationToNextStop, String[] changes) {
        super(stopName, nextStop, distanceToNextStop, durationToNextStop);
        assignChanges(changes);
    }

    public TransferStop(String busName, String[] changes) {
        super(busName);
        this.changes = changes;
    }

    private void assignChanges(String... p) {
        if (p.length > 0) {
            changes = new String[p.length];
            int i = 0;
            for (String pp : p) {
                changes[i++] = pp;
            }
        }
    }

    @Override
    public String toString() {
        String res = super.toString();
        res += "\nAvaible changes \n";
        if (changes == null) res += "- none";
        else for (String mojaNazwa : changes) res += mojaNazwa + "\n";
        return res;
    }
}
